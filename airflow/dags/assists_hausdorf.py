from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime
from datetime import date

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "assists_hausdorf", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )


#################################################################################
########################################################## Тут начинается скрипт

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    from sqlalchemy import create_engine

    def period(last_days):
        a=str(yesterday.getYesterday())
        a=datetime.strptime(a, '%Y-%m-%d')
        b=timedelta(days=last_days)
        dateFrom=a-b
        return(dateFrom)


    ##############################################################################################################
    #################################################### Вводные

    # Куда записать?
    dataset = "nla_hausdorf"

    # Получить данные по ассоциированным конверсиям за X дней:
    days = 180
    dateFrom = period(days).strftime("%Y-%m-%d")

    # Атрибутивное окно
    atr_window = 90

    # Запрос к Logs Api за 1 день
    mode = 30

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'11773411', 
        'token_ym':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['238923754','238923755'], # purchase если требуются данные по еком покупкам
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'hausdorf', # тег для куска данных. необходим для объединения с данными по рекламе
        }
    ]

    # Аккаунты яндекс директа, содержащие рекламные кампании
    accounts = [
        {'token_yd': 'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'login':'Hausdorf2-nla-185'}
    ]

    dateTo = str(yesterday.getYesterday())

    ##############################################################################################################
    #################################################### Яндекс Директ - рекламные материалы - ссылки

    assists_ads = pd.DataFrame()
    for i in accounts:
        direct_dataset = yd.get_hrefs(i['token_yd'], i['login'])
        assists_ads = pd.concat([assists_ads, direct_dataset], ignore_index=True)

    ##############################################################################################################
    #################################################### Яндекс Директ - рекламные материалы - шаблоны отслеживания, статусы и имена кампаний

    assists_campaigns = pd.DataFrame()
    for i in accounts:
        direct_dataset = yd.get_campaigns_params(i['token_yd'], i['login'])
        assists_campaigns = pd.concat([assists_campaigns, direct_dataset], ignore_index=True)

    tracking_columns = [col for col in assists_campaigns.columns if 'TrackingParams' in col]
    assists_campaigns['TrackingParams'] = assists_campaigns[tracking_columns].apply(lambda row: ''.join(row.dropna()), axis=1)

    ##############################################################################################################
    #################################################### Яндекс Метрика - Ассоциированные конверсии

    control_assists_camp = pd.DataFrame(columns=['date','utm_medium','utm_source','utm_campaign','tag'])
    control_assists_sour = pd.DataFrame(columns=['date','lastTrafficSource','source2','tag'])

    for i in counter:
        ass_camp = ym.associate_conv_camp(i['counter'],i['token_ym'],i['dateFrom'],dateTo,i['conv'],atr_window,mode)
        ass_camp['tag'] = i['tag']
        control_assists_camp = pd.concat([control_assists_camp, ass_camp], ignore_index=True)

        ass_sour = ym.associate_conv_sour(i['counter'],i['token_ym'],i['dateFrom'],dateTo,i['conv'],atr_window,mode)
        ass_sour['tag'] = i['tag']
        control_assists_sour = pd.concat([control_assists_sour, ass_sour], ignore_index=True)

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    mode_write = "replace" # перезапись данных "append" # добавление данных
    control_assists_camp.to_sql("control_assists_camp", engine, if_exists=mode_write)
    control_assists_sour.to_sql("control_assists_sour", engine, if_exists=mode_write)
    assists_ads.to_sql("assists_ads", engine, if_exists=mode_write)
    assists_campaigns.to_sql("assists_campaigns", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3