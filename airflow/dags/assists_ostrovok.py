from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime
from datetime import date

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine
from tapi_yandex_direct import YandexDirect

get_data = DAG(
    "assists_ostrovok", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=datetime(2023, 10, 8),
    schedule_interval='50 20 * * *' # когда запускать задачу, UTC (по МСК +3 часа будет)
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday
    import re

    from sqlalchemy import create_engine

    def period(last_days):
        a=str(yesterday.getYesterday())
        a=datetime.strptime(a, '%Y-%m-%d')
        b=timedelta(days=last_days)
        dateFrom=a-b
        return(dateFrom)

    ##############################################################################################################
    #################################################### Вводные   

    # Куда записать?
    dataset = "nla_ostrovok"

    # Получить данные по ассоциированным конверсиям за X дней:
    days = 90
    dateFrom = period(days).strftime("%Y-%m-%d")

    # Атрибутивное окно
    atr_window = 90

    # Запрос к Logs Api за 1 день
    mode = 30

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'4315831', 
        'token_ym':'y0_AgAAAABsqrbSAAPqugAAAADqhtuXR3i6xhn5TiGRkSNmiW1FbOCYsDs', 
        'conv':['22568385','1248586','purchase'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'ostrovok', # тег для куска данных. необходим для объединения с данными по рекламе
        }
    ]

    accounts = [
        {'token_yd': 'y0_AgAAAABwD_CdAAVNXQAAAADqau2cdhHLv6hDQSK11C3M5A620Sl9wQA',
         'login':'ostrovok-generator-nla-202'},

         {'token_yd': 'y0_AgAAAABwD_CdAAVNXQAAAADqau2cdhHLv6hDQSK11C3M5A620Sl9wQA',
         'login':'ostrovok-nl02-nla-42'}
    ]

    dateTo = str(yesterday.getYesterday())

    ##############################################################################################################
    #################################################### Яндекс Директ - рекламные материалы - ссылки

    assists_ads = pd.DataFrame()
    for i in accounts:
        direct_dataset = yd.get_hrefs(i['token_yd'], i['login'])
        assists_ads = pd.concat([assists_ads, direct_dataset], ignore_index=True)

    ##############################################################################################################
    #################################################### Яндекс Директ - рекламные материалы - шаблоны отслеживания, статусы и имена кампаний

    assists_campaigns = pd.DataFrame()
    for i in accounts:
        direct_dataset = yd.get_campaigns_params(i['token_yd'], i['login'])
        assists_campaigns = pd.concat([assists_campaigns, direct_dataset], ignore_index=True)

    tracking_columns = [col for col in assists_campaigns.columns if 'TrackingParams' in col]
    assists_campaigns['TrackingParams'] = assists_campaigns[tracking_columns].apply(lambda row: ''.join(row.dropna()), axis=1)

    ##############################################################################################################
    #################################################### Яндекс Метрика - Ассоциированные конверсии

    control_assists_camp = pd.DataFrame(columns=['date','utm_medium','utm_source','utm_campaign','tag'])
    control_assists_sour = pd.DataFrame(columns=['date','lastTrafficSource','source2','tag'])

    for i in counter:
        ass_camp = ym.associate_conv_camp(i['counter'],i['token_ym'],i['dateFrom'],dateTo,i['conv'],atr_window,mode)
        ass_camp['tag'] = i['tag']
        control_assists_camp = pd.concat([control_assists_camp, ass_camp], ignore_index=True)

        ass_sour = ym.associate_conv_sour(i['counter'],i['token_ym'],i['dateFrom'],dateTo,i['conv'],atr_window,mode)
        ass_sour['tag'] = i['tag']
        control_assists_sour = pd.concat([control_assists_sour, ass_sour], ignore_index=True)

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    mode_write = "replace" # перезапись данных "append" # добавление данных
    control_assists_camp.to_sql("control_assists_camp", engine, if_exists=mode_write)
    control_assists_sour.to_sql("control_assists_sour", engine, if_exists=mode_write)
    assists_ads.to_sql("assists_ads", engine, if_exists=mode_write)
    assists_campaigns.to_sql("assists_campaigns", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3