from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "old_clients_dashboards", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def oceanis_therm():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dateFrom = "2023-03-01"
    dataset = "old_oceanis_therm" # целевой датасет или база данных для записи
    name_client = "Океанис Терм"

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    # project_id = 'powerbi4rb'
    # json_path = r"patterns\\pbi4rb_gbq.json" # windows 10
    # json_path = r'/var/home/rb1/!py_script/patterns/pbi4rb_gbq.json' # linux

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    res_df_yd = pd.DataFrame()
    res_df_ym = pd.DataFrame()
    res_df_vk = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    goals_yd = ['291210068','291212676','291214181','291212298','291214202','291207597']

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'Oceanistherm-riverstart', 
            'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
            'dateFrom':dateFrom,
            'conv':goals_yd
        }
    ]

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'88269279', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':goals_yd,
        'dateFrom':dateFrom
        }
    ]

    vk_c = [
        {'uid':'1675401',
        'tkn':'8f841ba1c1bdc3d0a2ac0f790e4e26ac',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_old_client_report' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_old_client_report' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        # value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        # metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        # metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        res_df_ym = pd.concat([res_df_ym, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_old_client_report' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        res_df_yd = pd.concat([res_df_yd, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        res_df_vk = pd.concat([res_df_vk, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    res_df_yd.to_sql("yd_old_report", engine, if_exists=mode_write)
    res_df_ym.to_sql("ym_old_report", engine, if_exists=mode_write)
    res_df_vk.to_sql("vk_old_report", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def oceanis_aqua():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dateFrom = "2023-03-01"
    dataset = "old_oceanis_aqua" # целевой датасет или база данных для записи
    name_client = "Океанис Аква"

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    # project_id = 'powerbi4rb'
    # json_path = r"patterns\\pbi4rb_gbq.json" # windows 10
    # json_path = r'/var/home/rb1/!py_script/patterns/pbi4rb_gbq.json' # linux

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    res_df_yd = pd.DataFrame()
    res_df_ym = pd.DataFrame()
    res_df_vk = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    goals_yd = ['290534052','290532335','290531916','290746671','290531190','290534205']

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'Oceanisaquapark-riverstart', 
            'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
            'dateFrom':dateFrom,
            'conv':goals_yd
        }
    ]

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'88269306', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':goals_yd,
        'dateFrom':dateFrom
        }
    ]

    vk_c = [
        {'uid':'1675402',
        'tkn':'ce94646a116e2f6856139caa3c8c8e5d',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_old_client_report' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_old_client_report' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        # value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        # metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        # metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        res_df_ym = pd.concat([res_df_ym, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_old_client_report' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        res_df_yd = pd.concat([res_df_yd, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service','CampaignId']]
        res_df_vk = pd.concat([res_df_vk, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    res_df_yd.to_sql("yd_old_report", engine, if_exists=mode_write)
    res_df_ym.to_sql("ym_old_report", engine, if_exists=mode_write)
    res_df_vk.to_sql("vk_old_report", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def oceanis_regions():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dateFrom = "2023-03-01"
    dataset = "old_oceanis_regions" # целевой датасет или база данных для записи
    name_client = "Океанис Регионы"

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    # project_id = 'powerbi4rb'
    # json_path = r"patterns\\pbi4rb_gbq.json" # windows 10
    # json_path = r'/var/home/rb1/!py_script/patterns/pbi4rb_gbq.json' # linux

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    res_df_yd = pd.DataFrame()
    res_df_ym = pd.DataFrame()
    res_df_vk = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    goals_yd = ['290534052','290532335','290531916','290746671','290531190','290534205']

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'Oceanisaquapark-rf-riverstart', 
            'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
            'dateFrom':dateFrom,
            'conv':goals_yd
        }
    ]

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'88269306', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':goals_yd,
        'dateFrom':dateFrom
        }
    ]

    vk_c = [
        {'uid':'1675402',
        'tkn':'ce94646a116e2f6856139caa3c8c8e5d',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_old_client_report' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_old_client_report' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        # value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        # metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        # metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        res_df_ym = pd.concat([res_df_ym, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_old_client_report' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        res_df_yd = pd.concat([res_df_yd, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service','CampaignId']]
        res_df_vk = pd.concat([res_df_vk, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    res_df_yd.to_sql("yd_old_report", engine, if_exists=mode_write)
    res_df_ym.to_sql("ym_old_report", engine, if_exists=mode_write)
    res_df_vk.to_sql("vk_old_report", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

oceanis_therm_data = PythonOperator(
    task_id = 'get_oceanis_therm_data',
    python_callable=oceanis_therm,
    dag=get_data
    )

oceanis_aqua_data = PythonOperator(
    task_id = 'get_oceanis_aqua_data',
    python_callable=oceanis_aqua,
    dag=get_data
    )

oceanis_regions_data = PythonOperator(
    task_id = 'get_oceanis_regions_data',
    python_callable=oceanis_regions,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

oceanis_therm_data >> oceanis_aqua_data >> oceanis_regions_data