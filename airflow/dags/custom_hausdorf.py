from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "custom_hausdorf", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday
    import re

    from sqlalchemy import create_engine

    dateFrom = "2023-08-20"

    # Директ - расходы. Можно удалить если источник не используется

    yd_stats = [
        {'login':'Hausdorf2-nla-185', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'dateTo': str(yesterday.getYesterday()),
            'conv':[],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'Hausdorf'
        }
    ]

    ##############################################################################################################
    #################################################### Яндекс Директ - статистика

    ydirect_stats = pd.DataFrame()

    # Поля для данных директа

    report_fields = "Date,CampaignName,CampaignId,Impressions,Clicks,Cost,AvgImpressionPosition"

    for i in yd_stats:
        report_fields = list(report_fields.split(","))
        if i['conv'] == [] or i['conv'] == ['']:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], '', i['dateFrom'], i['dateTo'], 60, report_fields)
        else:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], i['dateTo'], 60, report_fields)
        
        if i['camp_filter'] == [] or i['camp_filter'] == ['']:
            pass
        else:
            direct_dataset = direct_dataset[direct_dataset['CampaignId'].isin(i['camp_filter'])]

        direct_dataset['tag'] = i['tag']
        ydirect_stats = pd.concat([ydirect_stats, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    # Функция для извлечения названия бренда
    def extract_brand(campaign_name):
        pattern = r'(?<=_)[A-Z\s]+(?=_)'  # Изменили регулярное выражение
        matches = re.findall(pattern, campaign_name)
        if matches:
            return matches[0]
        return 'Unknown'

    # Создание нового столбца с извлеченными названиями брендов
    ydirect_stats['brand'] = ydirect_stats['CampaignName'].apply(extract_brand)

    #################################### Яндекс Директ - статусы РК
    yd_states = pd.DataFrame()
    for i in yd_stats:
        get_states = yd.get_ads(i['token'], i['login'])
        get_states = get_states.groupby('CampaignId')['State'].first().reset_index()
        get_states['tag'] = i['tag']
        yd_states = pd.concat([yd_states, get_states], ignore_index=True)

    # PostgreSQL - коннект

    dataset = "nla_hausdorf"
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    ydirect_stats.to_sql("custom_client_table", engine, if_exists=mode_write)
    yd_states.to_sql("custom_client_states", engine, if_exists=mode_write)

    # отправляет данные в подключенную GS
    def send_to_gs(sh_name, df, json_path, sheet_id):
        import gspread  # для работы с GS
        from gspread_dataframe import set_with_dataframe  # функция отправки таблицы в GS
        from google.oauth2.credentials import Credentials  # для авторизации в Google
        
        #-# авторизация и подключение к листу (создание его, при отсутствии)
        gc = gspread.service_account(filename=r'/home/alex/airflow/dags/patterns/python-data-to-sheets-857407b48471.json')  # местоположение ключа oauth2
        sh = gc.open_by_key(sheet_id)  # id таблицы

        # sh_name = название листа, на который будут записаны данные 
        # df = данные, которые нужно отправить
        # если указанного листа нет, то он будет создан
        try:
            worksheet = sh.worksheet(sh_name)
        except:
            worksheet = sh.add_worksheet(title=sh_name,
                                        rows="10",
                                        cols="100")

        # очистка листа
        worksheet.clear()

        # запись на лист
        set_with_dataframe(worksheet=worksheet,
                        dataframe=df,
                        include_index=False,
                        include_column_header=True,
                        resize=True)

    # ключ Json от GCP (oauth2)
    json_path = 'python_sheets.json'

    # ID таблицы GS
    sheet_id = '16b6urChFcin14D4KeDL6AxOuacv85BSBaH8CvcGlhus'

    # Название листа, на который будут записаны данные
    sheet_name = 'test'

    send_to_gs(sh_name=sheet_name,  # название листа, в который будут записаны данные
            df=ydirect_stats,  # таблица, которая будет отправлена в GS
            json_path=json_path,  # путь к json авторизации
            sheet_id=sheet_id)  # идентификатор таблицы GS

    a = 'Success!'
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3