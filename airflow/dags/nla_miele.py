from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "nla_miele", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=datetime(2023, 9, 29),
    schedule_interval='50 3 * * *' # когда запускать задачу, UTC (по МСК +3 часа будет)
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import requests
    import pandas as pd
    import xml.etree.ElementTree as ET
    import re
    from sqlalchemy import create_engine

    #########################################################################################################
    ####################################################################### Вводные данные

    # Куда записать?
    dataset = "nla_zoomos_ret"
    mode_write = "replace" # перезапись данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    accounts = [
            {
            'site':'miele-rus.ru', # тег для куска данных. необходим для объединения с данными по рекламе
            'key':'miele-rus.ru-wJw$SHFW3',
            'feed': 'https://miele-rus.ru/xml_feeds/context/xml_1.xml' # фид в зумусе
            }
        ]

    ##############################################################################################################
    #################################################### Цены конкурентов из зумус

    zoomos_stats = pd.DataFrame()

    for i in accounts:
        url_api = "https://api.zoomos.by/competitors/sites?key=" + i['key'] + "&onlyWithCompetitors=1&showDate=1"
        response = requests.get(url_api)
        stats = response.json()
        df = pd.json_normalize(stats)
        # Развернуть столбец 'competitors' и столбцы внутри него
        df = df.explode('competitors').reset_index(drop=True)
        df = pd.concat([df, pd.json_normalize(df['competitors']).add_prefix('competitors.')], axis=1)
        df.drop('competitors', axis=1, inplace=True)
        df['site'] = i['site']
        zoomos_stats = pd.concat([zoomos_stats,df], ignore_index=True)
        # Функция для извлечения названия бренда
        def extract_brand(campaign_name):
            pattern = r'(?<=_)[A-Z\s]+(?=_)'  # Изменили регулярное выражение
            matches = re.findall(pattern, campaign_name)
            if matches:
                return matches[0]
            return 'Unknown'

    ##############################################################################################################
    #################################################### Фиды на сайтах, загруженные в зумус
    feed_all = pd.DataFrame()

    for i in accounts:
        url = i['feed']
        response = requests.get(url)
        xml_data = response.content
        # Разбираем XML
        tree = ET.fromstring(xml_data)

        # Создаем пустые списки для данных
        offer_ids = []
        parent_category_ids = []
        parent_category_names = []
        offer_urls = []

        # Извлекаем данные о категориях из фида
        categories_data = []
        for category in tree.findall('.//categories/category'):
            category_id = category.get('id')
            parent_id = category.get('parentId')
            name = category.text
            categories_data.append({'id': category_id, 'parentId': parent_id, 'name': name})

        # Проходим по элементам XML и добавляем данные в списки
        for offer in tree.findall('.//offer'):
            offer_id = offer.get('id')
            category_id_elem = offer.find('categoryId')

            if category_id_elem is not None:
                category_id = category_id_elem.text

                # Проверяем, есть ли категория в списке
                matching_category = next((cat for cat in categories_data if cat['id'] == category_id), None)

                if matching_category is not None:
                    parent_category_id = matching_category['parentId'] if matching_category['parentId'] is not None else category_id
                    offer_url = offer.find('url').text if offer.find('url') is not None else None
                    offer_ids.append(offer_id)
                    parent_category_ids.append(parent_category_id)
                    offer_urls.append(offer_url)
                else:
                    # Возможно, вы захотите добавить обработку случая, когда категория не найдена
                    pass

        # Создаем DataFrame из списков
        df = pd.DataFrame({
            'offer_id': offer_ids,
            'parent_category_id': parent_category_ids,
            'url': offer_urls
        })

        # Объединяем DataFrame с категориями по столбцам id и parentId
        categories_df = pd.DataFrame(categories_data)
        merged_df = pd.merge(df, categories_df, left_on='parent_category_id', right_on='id', how='left')

        # Оставляем нужные столбцы
        result_df = merged_df[['offer_id', 'parent_category_id', 'name', 'url']].rename(columns={'name': 'parent_category_name'})


        def extract_domain_with_regex(url):
            # Регулярное выражение для извлечения домена из URL
            pattern = re.compile(r'https?://([^/?]+)')

            match = pattern.match(url)
            if match:
                return match.group(1)
            else:
                return None
        
        domain = extract_domain_with_regex(url)
        result_df['site'] = domain

        feed_all = pd.concat([feed_all,result_df], ignore_index=True)

    ##################################### Для K50
    ads_table = zoomos_stats
    selected_columns = ['productId','price','competitors.price']
    ads_table = ads_table[selected_columns]

    selected_columns = ['offer_id','url']
    urls_table = feed_all[selected_columns]

    ads_table = pd.merge(ads_table, urls_table, left_on='productId', right_on='offer_id', how='left')
    ads_table = ads_table.astype({'competitors.price': int})
    # Группировка данных
    ads_table = ads_table.groupby('productId', as_index=False).agg({
        'url': 'first',
        'price': 'first',
        'competitors.price': 'min',
    })
    ads_table = ads_table.dropna(subset='url')
    ads_table = ads_table.rename(columns={'productId': 'Id'})

    # отправляет данные в подключенную GS
    def send_to_gs(sh_name, df, json_path, sheet_id):
        import gspread  # для работы с GS
        from gspread_dataframe import set_with_dataframe  # функция отправки таблицы в GS
        from google.oauth2.credentials import Credentials  # для авторизации в Google
        
        #-# авторизация и подключение к листу (создание его, при отсутствии)
        gc = gspread.service_account(filename=r'/home/alex/airflow/dags/patterns/python-data-to-sheets-857407b48471.json')  # местоположение ключа oauth2
        sh = gc.open_by_key(sheet_id)  # id таблицы

        # sh_name = название листа, на который будут записаны данные 
        # df = данные, которые нужно отправить
        # если указанного листа нет, то он будет создан
        try:
            worksheet = sh.worksheet(sh_name)
        except:
            worksheet = sh.add_worksheet(title=sh_name,
                                        rows="10",
                                        cols="100")

        # очистка листа
        worksheet.clear()

        # запись на лист
        set_with_dataframe(worksheet=worksheet,
                        dataframe=df,
                        include_index=False,
                        include_column_header=True,
                        resize=True)

    # ключ Json от GCP (oauth2)
    json_path = 'python_sheets.json'

    # ID таблицы GS
    sheet_id = '1g2W-6OB6CmA_5rrn8_D0I5aqIqPdv-wawUj98bbtWAM'

    # Название листа, на который будут записаны данные
    sheet_name = 'data'

    send_to_gs(sh_name=sheet_name,  # название листа, в который будут записаны данные
            df=ads_table,  # таблица, которая будет отправлена в GS
            json_path=json_path,  # путь к json авторизации
            sheet_id=sheet_id)  # идентификатор таблицы GS

    ##############################################################################################################
    #################################################### Пишем данные в БД

    zoomos_stats.to_sql("zoomos_stats", engine, if_exists=mode_write)
    result_df.to_sql("feed_data", engine, if_exists=mode_write)

    a = 'Success!'
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3