from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "riverstart_agency", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def clean():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    dataset = "agency_riverstart" # целевой датасет или база данных для записи
    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    clean_frame = pd.DataFrame()

    mode_write = "replace" # перезапись данных
    clean_frame.to_sql("agency_costs", engine, if_exists=mode_write)
    clean_frame.to_sql("agency_conv", engine, if_exists=mode_write)


def mhid():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "replace" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "M.House"
    dateFrom = "2022-12-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'23382340', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['221844571','221844344','221844334','62246119','59192935','59192911','41972125','227067489'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'mhouse-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    # VK - click.ru расходы. Можно удалить если источник не используется
    vk_c = [
        {'uid':'1578482',
        'tkn':'578105b12a56e031e91da4addeb6d433',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ##############################################################################
    ############################################################### VK Ads CLICK.RU
    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def mst():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "MST"
    dateFrom = "2023-03-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'15893314', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['237815756','237816036'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'mst-software', 
        'token':'y0_AgAAAABozenWAAVNXQAAAADeqz5ZDYlK6qPKRBaMBbOVwV6rB-nUF9c',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def intra():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "МЦ Интра"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'55226488', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':["273937979","159281410","57342526"],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'mcintra-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def escaper():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Escaper"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'67474306', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':["147801415","147818518","147819010","147820939","147821425", "155879743"],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'escaper-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)


def lider():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Лидер НН"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'24632501', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['255910880','255910817'],
        'dateFrom':dateFrom
        },
        {'counter':'91468104', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['273911120'],
        'dateFrom':dateFrom
        },
        {'counter':'35001410', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['275648988','265653143'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'franchise-monolite-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    # MT - click.ru расходы. Можно удалить если источник не используется
    mt_c = [
        {'uid':'1578544',
        'tkn':'a7caf462f7cebbcb9c7f7a47af4d9fa2',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ##############################################################################
    ############################################################### MyTarget CLICK.RU
    for i in mt_c:
        mt_click_dataset = click.click_mt(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        mt_click_dataset['Client'] = name_client
        mt_click_dataset = mt_click_dataset.rename(columns={'account':'Login'})
        mt_click_dataset = mt_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, mt_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def bic_tower():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Бик Tower"
    dateFrom = "2022-10-19"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'85824073', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['255655713','253557091','276202694','253535307','253535175'],
        'dateFrom':dateFrom
        }
    ]

    # MT - click.ru расходы. Можно удалить если источник не используется
    mt_c = [
        {'uid':'1641088',
        'tkn':'e837f3c30434a9cf5c855c4fb10825db',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### MyTarget CLICK.RU
    for i in mt_c:
        mt_click_dataset = click.click_mt(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        mt_click_dataset['Client'] = name_client
        mt_click_dataset = mt_click_dataset.rename(columns={'account':'Login'})
        mt_click_dataset = mt_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, mt_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def svetpro():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Светпро"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'464542', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        },
        {'counter':'25165754', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        },
        {'counter':'55963255', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        },
        {'counter':'14796532', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        },
        {'counter':'20943679', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'svetpro-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        },
        {'login':'petrasvet-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        },
        {'login':'opora24-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        },
        {'login':'armadasvet-ret-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        },
        {'login':'stksvet-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        # goals_ym = ym.convert_goals_s(i['conv'])
        # report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def kvadro():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Квадро-НН"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'47401369', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['294150455','294150457','294150546','294150549','294150655'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'kvadronn-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        # goals_ym = ym.convert_goals_s(i['conv'])
        # report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def beketov():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "СЦ Бекетов"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'52902262', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['265921332'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'ppc-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def pincher():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Пинчер СРМ"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'68370820', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':[''],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'pinchercrm-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        },
        {'login':'pinscher-sales-client', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        # goals_ym = ym.convert_goals_s(i['conv'])
        # report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def smartroof():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Смартруф"
    dateFrom = "2023-01-01"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'62911963', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['104478397','104478586','116933908','134431672','104473732'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'smart-roof', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить


    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

    a = "Success!"
    return(a)

def oceanis_aqua():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Oceanis Aqua"
    dateFrom = "2023-04-05"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'88269306', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['290534052','290532335','290531916'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'Oceanisaquapark-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    # VK - click.ru расходы. Можно удалить если источник не используется
    vk_c = [
        {'uid':'1675402',
        'tkn':'ce94646a116e2f6856139caa3c8c8e5d',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ##############################################################################
    ############################################################### VK Ads CLICK.RU
    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def oceanis_therm():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Oceanis Therm"
    dateFrom = "2023-04-05"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'88269279', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['291210068','291212676','291214181'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'Oceanistherm-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    # VK - click.ru расходы. Можно удалить если источник не используется
    vk_c = [
        {'uid':'1675401',
        'tkn':'8f841ba1c1bdc3d0a2ac0f790e4e26ac',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ##############################################################################
    ############################################################### VK Ads CLICK.RU
    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def yastrebov():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Ястребов"
    dateFrom = "2023-04-11"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'93164635', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['291154971','291156484','291156593','291156506','291157061'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'yastrebov-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    # VK - click.ru расходы. Можно удалить если источник не используется
    vk_c = [
        {'uid':'1681243',
        'tkn':'91ea44db8c926475b4ebfbc1b1170212',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ##############################################################################
    ############################################################### VK Ads CLICK.RU
    for i in vk_c:
        vk_click_dataset = click.click_vk(i['dateFrom'],dateTo,i['uid'],i['tkn'])
        vk_click_dataset['Client'] = name_client
        vk_click_dataset = vk_click_dataset.rename(columns={'account':'Login'})
        vk_click_dataset = vk_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
        agency_costs = pd.concat([agency_costs, vk_click_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def smart_metall():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    ################################################################################################# Нейминг клиента
    name_client = "Смарт Металл"
    dateFrom = "2023-04-20"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'83030433', 
        'token':'AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E', 
        'conv':['209505223','293266577'],
        'dateFrom':dateFrom
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'smart-metal-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    for i in counter:
        fields_dim = 'dim_agency' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        fields_met = 'met_agency' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

        report_fields_dim = config['yandex_metrika_names'][fields_dim]
        report_fields_met = config['yandex_metrika_names'][fields_met]

        goals_ym = ym.convert_goals_s(i['conv'])
        report_fields_met = report_fields_met + ',' + goals_ym

        metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
        value_cols = metrika_dataset.filter(like='ym_s_goal').columns
        metrika_dataset['Macroconversions'] = metrika_dataset[value_cols].sum(axis=1)
        metrika_dataset = metrika_dataset.drop(columns=value_cols)
        metrika_dataset['Client'] = name_client
        agency_conv = pd.concat([agency_conv, metrika_dataset], ignore_index=True)

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)
    a = "Success!"
    return(a)

def rusgeosint():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    # Нейминг клиента
    name_client = "Русгеосинт"
    dateFrom = "2023-01-01"

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'rusgeosintriverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

# def globalgeosint():
#     sys.path.append(r'/home/alex/airflow/dags/patterns')
#     sys.path.append(r'/home/alex/airflow/dags/')
#     config = configparser.ConfigParser()
#     config.read(r'/home/alex/airflow/dags/__fields.ini')
#     config_db = configparser.ConfigParser()
#     config_db.read(r'/home/alex/airflow/dags/__settings.ini')

#     # папка patterns должна лежать рядом с данным скриптом
#     import patterns.yd as yd
#     import patterns.ym as ym
#     import patterns.click as click
#     import patterns.yesterday as yesterday

#     ###########################################################################################
#     ################################################################ укажите параметры скрипта
#     # начальная дата
#     dateTo = str(yesterday.getYesterday()) #конечная дата
#     dataset = "agency_riverstart" # целевой датасет или база данных для записи

#     mode_write = "append" # добавление данных

#     # PostgreSQL - коннект
#     engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

#     ########################################################################################
#     ################################################################ Итоговые датафреймы (в них собираются данные)
#     agency_costs = pd.DataFrame()
#     agency_conv = pd.DataFrame()

#     # Нейминг клиента
#     name_client = "Глобалгеосинт"
#     dateFrom = "2023-01-01"

#     # Директ - расходы. Можно удалить если источник не используется
#     yd_clientLogin = [
#         {'login':'globalgeosint-riverstart', 
#         'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
#         'dateFrom':dateFrom
#         }
#     ]

#     ga_c = [
#         {'uid':'1654462',
#         'tkn':'a4da58614055acdf55f333d3916888a2',
#         'dateFrom':dateFrom
#         }
#     ]

#     ##############################################################################
#     ############################################################### Яндекс директ
#     for i in yd_clientLogin:
#         fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
#         report_fields = list(config['yandex_direct_names'][fields].split(","))
#         direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
#         direct_dataset['Client'] = name_client
#         direct_dataset['TrafficType'] = 'Контекст'
#         direct_dataset['service'] = 'YANDEX_DIRECT'
#         agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

#     ##############################################################################
#     ############################################################### Google Ads CLICK.RU
#     for i in ga_c:
#         ga_click_dataset = click.click_ga(i['dateFrom'],dateTo,i['uid'],i['tkn'])
#         ga_click_dataset['Client'] = name_client
#         ga_click_dataset = ga_click_dataset.rename(columns={'account':'Login'})
#         ga_click_dataset = ga_click_dataset[['Date','Impressions','Clicks','Cost','Login','Client','TrafficType','service']]
#         ga_click_dataset = click.usd_to_rub(i['dateFrom'],dateTo,ga_click_dataset)
#         agency_costs = pd.concat([agency_costs, ga_click_dataset], ignore_index=True)

#     ###################################################################################################
#     ################################################################ Отправляем полученные данные - POSTGRESQL
#     agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
#     agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

def kazakhstangeosint():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    # Нейминг клиента
    name_client = "Казахстангеосинт"
    dateFrom = "2023-01-01"

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'kazakhstangeosint', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)

def pimu():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_riverstart" # целевой датасет или база данных для записи

    mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    agency_costs = pd.DataFrame()
    agency_conv = pd.DataFrame()

    # Нейминг клиента
    name_client = "ПИМУ"
    dateFrom = "2023-01-01"

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'pimunn-riverstart', 
        'token':'AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE',
        'dateFrom':dateFrom
        }
    ]

    ##############################################################################
    ############################################################### Яндекс директ
    for i in yd_clientLogin:
        fields = 'yd_agency' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))
        direct_dataset = yd.get_data_yd(i['token'], i['login'], "", i['dateFrom'], dateTo, 60, report_fields) # функция обращается к api директа и собирает данные
        direct_dataset['Client'] = name_client
        direct_dataset['TrafficType'] = 'Контекст'
        direct_dataset['service'] = 'YANDEX_DIRECT'
        agency_costs = pd.concat([agency_costs, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    agency_costs.to_sql("agency_costs", engine, if_exists=mode_write)
    agency_conv.to_sql("agency_conv", engine, if_exists=mode_write)


# Для каждой функции копируем блок operation и вписываем функцию в python_callable

clean_table = PythonOperator(
    task_id = 'clean_dataframe',
    python_callable=clean,
    dag=get_data
    )

mhid_data = PythonOperator(
    task_id = 'get_mhid_data',
    python_callable=mhid,
    dag=get_data
    )

intra_data = PythonOperator(
    task_id = 'get_intra_data',
    python_callable=intra,
    dag=get_data
    )

escaper_data = PythonOperator(
    task_id = 'get_escaper_data',
    python_callable=escaper,
    dag=get_data
    )

lider_data = PythonOperator(
    task_id = 'get_lider_data',
    python_callable=lider,
    dag=get_data
    )

bik_data = PythonOperator(
    task_id = 'get_bik_data',
    python_callable=bic_tower,
    dag=get_data
    )

svetpro_data = PythonOperator(
    task_id = 'get_svetpro_data',
    python_callable=svetpro,
    dag=get_data
    )

kvadro_data = PythonOperator(
    task_id = 'get_kvadro_data',
    python_callable=kvadro,
    dag=get_data
    )

beketov_data = PythonOperator(
    task_id = 'get_beketov_data',
    python_callable=beketov,
    dag=get_data
    )

pincher_data = PythonOperator(
    task_id = 'get_pincher_data',
    python_callable=pincher,
    dag=get_data
    )

smartroof_data = PythonOperator(
    task_id = 'get_smartroof_data',
    python_callable=smartroof,
    dag=get_data
    )

oceanis_aqua_data = PythonOperator(
    task_id = 'get_oceanis_aqua_data',
    python_callable=oceanis_aqua,
    dag=get_data
)

oceanis_therm_data = PythonOperator(
    task_id = 'get_oceanis_therm_data',
    python_callable=oceanis_therm,
    dag=get_data
)

yastrebov_data = PythonOperator(
    task_id = 'get_yastrebov_data',
    python_callable=yastrebov,
    dag=get_data
)

smartmetall_data = PythonOperator(
    task_id = 'get_smartmetall_data',
    python_callable=smart_metall,
    dag=get_data
)

rusgeosint_data = PythonOperator(
    task_id = 'get_rusgeosint_data',
    python_callable=rusgeosint,
    dag=get_data
)

# globalgeosint_data = PythonOperator(
#     task_id = 'get_globalgeosint_data',
#     python_callable=globalgeosint,
#     dag=get_data
# )

kazakhstangeosint_data = PythonOperator(
    task_id = 'get_kazakhstangeosint_data',
    python_callable=kazakhstangeosint,
    dag=get_data
)

pimu_data = PythonOperator(
    task_id = 'get_pimu_data',
    python_callable=pimu,
    dag=get_data
)

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

clean_table >> mhid_data >> intra_data >> escaper_data >> lider_data >> bik_data >> svetpro_data >> kvadro_data >> beketov_data >> pincher_data >> smartroof_data >> oceanis_aqua_data >> oceanis_therm_data >> yastrebov_data >> smartmetall_data >> rusgeosint_data >> kazakhstangeosint_data >> pimu_data