import sys

sys.path.append('/opt/airflow/')

from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "nla_ab_test", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    from sqlalchemy import create_engine

    ###########################################################################################
    ################################################################ укажите параметры скрипта

    dataset = "nla_dataset" # целевой датасет или база данных для записи

    ym_tkn = "y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k"

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ###########################################################################################
    ################################################################## формируем список тестов

    test_list = [
        {'tag':'Портфели к-50 VS Автостратегия ЯД',
        'counter':'14490490', # счетчик в котором лежит тест
        'token': "y0_AgAAAABwD_CdAAPqugAAAADqauuxuGZYs05iSTarayZaw0nQf8WcCAA", 
        'conv':['1895053'], # конверсии теста для определения эффективности
        'dateFrom':"2023-07-13", # дата начала теста
        'dateTo':"2023-08-13", # дата конца теста
        'experiment':"132513", # id теста в яндекс аудиториях
        'login':'ret-nla',
        'campaigns': "",
        'ids': ['91464203','91464342','91468968','91469003'] # id рекламных кампаний участвующих в тесте 
        },

        {'tag':'ручная стратегия / макс конв с оплатой за клик + нед бюджет',
        'counter':'11773411', # счетчик в котором лежит тест
        'token': ym_tkn, 
        'conv':['1067641','297985150'], # конверсии теста для определения эффективности
        'dateFrom':"2023-08-21", # дата начала теста
        'dateTo':"", # дата конца теста. Если точно даты нет, ничего не указываем
        'experiment':"136459", # id теста в яндекс аудиториях
        'login':'Hausdorf2-nla-185',
        'campaigns': "a/b-test",
        'ids': [''] # id рекламных кампаний участвующих в тесте 
        },

        {'tag':'макс конв с оплатой за клик + нед бюджет / макс конв с оплатой за клик + нед бюджет + оптимизация к50',
        'counter':'18704212', # счетчик в котором лежит тест
        'token': ym_tkn, 
        'conv':['1892077','1892041','261917396'], # конверсии теста для определения эффективности
        'dateFrom':"2023-08-28", # дата начала теста
        'dateTo':"", # дата конца теста. Если точно даты нет, ничего не указываем
        'experiment':"137219", # id теста в яндекс аудиториях
        'login':'ret-nla',
        'campaigns': 'ab_test',
        'ids': [''] # id рекламных кампаний участвующих в тесте 
        },

        {'tag':'ручная стратегия / оптимизация через сервис андата',
        'counter':'11881159', # счетчик в котором лежит тест
        'token': 'y0_AgAAAABWxo0VAAPqugAAAADrYiQ2_D-RcRpbT_CywpKgepvVP5vQvCM', 
        'conv':['33012822'], # конверсии теста для определения эффективности
        'dateFrom':"2023-08-28", # дата начала теста
        'dateTo':"", # дата конца теста. Если точно даты нет, ничего не указываем
        'experiment':"137339", # id теста в яндекс аудиториях
        'login':'travelata-nla',
        'campaigns': 'ab_test',
        'ids': [''] # id рекламных кампаний участвующих в тесте 
        }
    ]

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)

    ab_tests_stat = pd.DataFrame(columns=['Date','directID','lastSignDirectOrderName','experimentAB','Clicks','RUBAdCost','BounceRate','pageDepth','avgVisitDurationSeconds','ecommercePurchases','ecommerceRUBRevenue','Login','tag'])
    ab_tests_stat.to_sql("ab_tests_stat", engine, if_exists=mode_write)
    ab_goals_stat = pd.DataFrame(columns=['Date','directID','lastSignDirectOrderName','experimentAB','Login','tag','count','id','name'])
    ab_goals_stat.to_sql("ab_goals_stat", engine, if_exists=mode_write)

    ##############################################################################################################
    #################################################### Яндекс Метрика
    from tapi_yandex_metrika import exceptions

    for i in test_list:
        report_fields_dim = "ym:ad:date,ym:ad:directID,ym:ad:lastSignDirectOrderName,ym:ad:experimentAB" + i['experiment']
        report_fields_met = "ym:ad:clicks,ym:ad:RUBAdCost,ym:ad:bounceRate,ym:ad:pageDepth,ym:ad:avgVisitDurationSeconds,ym:ad:ecommercePurchases,ym:ad:ecommerceRUBRevenue"

        if i['conv'] == [] or i['conv'] == ['']:
            pass
        else:
            goals_ym = ym.convert_goals_ad(i['conv'])
            report_fields_met = report_fields_met + ',' + goals_ym

        if i['dateTo'] == "" or i['dateTo'] == '':
            while True:
                try:
                    dateTo = str(yesterday.getYesterday())
                    metrika_dataset = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
                    break
                except exceptions.YandexMetrikaClientError as e:
                    if e.code == 400 and "Wrong parameter" in e.message:
                        report_fields_met = "ym:ad:clicks,ym:ad:RUBAdCost,ym:ad:bounceRate,ym:ad:pageDepth,ym:ad:avgVisitDurationSeconds"
                        report_fields_met = report_fields_met + ',' + goals_ym
                        metrika_dataset = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
                    else:
                        print("Произошла другая ошибка:", e)
                        break  # Выход из бесконечного цикла при другой ошибке
        else:
            while True:
                try:
                    metrika_dataset = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],i['dateTo'],report_fields_met,report_fields_dim)
                    break
                except exceptions.YandexMetrikaClientError as e:
                    if e.code == 400 and "Wrong parameter" in e.message:
                        report_fields_met = "ym:ad:clicks,ym:ad:RUBAdCost,ym:ad:bounceRate,ym:ad:pageDepth,ym:ad:avgVisitDurationSeconds"
                        report_fields_met = report_fields_met + ',' + goals_ym
                        metrika_dataset = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],i['dateTo'],report_fields_met,report_fields_dim)
                    else:
                        print("Произошла другая ошибка:", e)
                        break  # Выход из бесконечного цикла при другой ошибке


        metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_ad_", ""), inplace=True)
        
        original_columns = metrika_dataset.columns

        new_columns = {}

        for col in original_columns:
            if col.startswith('experimentAB'):
                new_col = 'experimentAB'
            else:
                new_col = col
            new_columns[col] = new_col

        metrika_dataset.rename(columns=new_columns, inplace=True)

        metrika_dataset['directID'] = metrika_dataset['directID'].str.replace('N-', '')

        if i['campaigns'] == "" or i['campaigns'] == '':
            pass
        else:
            metrika_dataset['lastSignDirectOrderName'] = metrika_dataset['lastSignDirectOrderName'].str.lower()
            metrika_dataset = metrika_dataset[metrika_dataset['lastSignDirectOrderName'].str.contains(i['campaigns'], case=False)]

        if i['ids'] == [] or i['ids'] == ['']:
            pass
        else:
            metrika_dataset = metrika_dataset[metrika_dataset['directID'].isin(i['ids'])]

        if metrika_dataset.shape[0] > 0:
            report_fields_dim = "ym:s:date,ym:s:experimentAB" + i['experiment'] + ",ym:s:experimentAB" + i['experiment'] + "Share,ym:s:experimentAB" + i['experiment'] + "Size"
            report_fields_met = "ym:s:visits"

            if i['dateTo'] == "" or i['dateTo'] == '':
                dateTo = str(yesterday.getYesterday())
                metrika_segments = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
            else:
                metrika_segments = ym.get_ym_stat(i['counter'],i['login'],i['token'],i['dateFrom'],i['dateTo'],report_fields_met,report_fields_dim)


            original_columns = metrika_segments.columns

            new_columns = {}

            for col in original_columns:
                if col.startswith('ym_s_experimentAB') and not any(forbidden_word in col for forbidden_word in ["Size", "Share"]):
                    new_col = 'experimentAB'
                elif 'Size' in col:
                    new_col = 'experimentAB_Size'
                elif 'Share' in col:
                    new_col = 'experimentAB_Share'
                else:
                    new_col = col
                new_columns[col] = new_col

            metrika_segments.rename(columns=new_columns, inplace=True)
            metrika_segments = metrika_segments.drop_duplicates(subset='experimentAB')
            selected_columns = ['experimentAB','experimentAB_Size','experimentAB_Share']
            metrika_segments = metrika_segments[selected_columns]
            
            metrika_dataset = metrika_dataset.merge(metrika_segments, how='left', left_on='experimentAB', right_on='experimentAB')

            metrika_dataset['tag'] = i['tag']
            # Создание списка столбцов, содержащих "Conversions_"
            conversion_columns = [col for col in metrika_dataset.columns if "Conversions_" in col]

            # Список для хранения новых строк
            new_rows = []

            # Итерация по каждой строке и каждому столбцу "Conversions_"
            for _, row in metrika_dataset.iterrows():
                for col in conversion_columns:
                    login = row["Login"]
                    tag = row["tag"]
                    count = row[col]
                    name_col = col
                    new_row = row[["Date", "directID", "lastSignDirectOrderName", "experimentAB",'experimentAB_Size','experimentAB_Share']].tolist()
                    new_row.extend([login, tag, count, name_col])
                    new_rows.append(new_row)

            # Создание нового датафрейма из списка новых строк
            columns = ["Date", "directID", "lastSignDirectOrderName", "experimentAB", 'experimentAB_Size','experimentAB_Share',"Login", "tag", "count", "Name col"]
            goals_stat = pd.DataFrame(new_rows, columns=columns)
            goals_stat['Name col'] = goals_stat['Name col'].str.extract(r'(\d+)')
            
            goals_list = goals_stat
            goals_list = goals_list.drop_duplicates(subset='Name col')
            goals_list = goals_list['Name col'].tolist()
            
            print(metrika_dataset.shape[0])
            goals_table = pd.DataFrame()
            for _g in goals_list:
                a = ym.get_ym_goals(i['counter'],i['token'],_g)
                selected_columns = ['id', 'name']
                a = a[selected_columns]
                goals_table = pd.concat([goals_table, a], ignore_index=True)

            goals_table = goals_table.astype({'id':'string'})
            goals_stat = goals_stat.astype({'Name col':'string'})

            goals_stat = goals_stat.merge(goals_table, how='left', left_on='Name col', right_on='id')
            
            selected_columns = ['Date','directID','lastSignDirectOrderName','experimentAB','experimentAB_Size','experimentAB_Share','Login','tag','count','id','name']
            goals_stat = goals_stat[selected_columns]


            if 'ecommercePurchases' in metrika_dataset.columns:
                selected_columns = ['Date','directID','lastSignDirectOrderName','experimentAB','experimentAB_Size','experimentAB_Share','Clicks','RUBAdCost','BounceRate','pageDepth','avgVisitDurationSeconds','ecommercePurchases','ecommerceRUBRevenue','Login','tag']
                metrika_dataset = metrika_dataset[selected_columns]
            else:
                selected_columns = ['Date','directID','lastSignDirectOrderName','experimentAB','experimentAB_Size','experimentAB_Share','Clicks','RUBAdCost','BounceRate','pageDepth','avgVisitDurationSeconds','Login','tag']
                metrika_dataset = metrika_dataset[selected_columns]
        else:
            selected_columns = ['Date','directID','lastSignDirectOrderName','Clicks','RUBAdCost','BounceRate','pageDepth','avgVisitDurationSeconds','Login']
            metrika_dataset = metrika_dataset[selected_columns]
    
    ####################################################################################################
    ########################################################################### Запись данных в БД

    ab_tests_stat.to_sql("ab_tests_stat", engine, if_exists=mode_write)
    ab_goals_stat.to_sql("ab_goals_stat", engine, if_exists=mode_write)

    a = 'Success!'
    return(a)


# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3