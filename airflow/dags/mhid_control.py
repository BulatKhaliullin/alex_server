from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "mhid_control", # Название задачи в airflow
    start_date=days_ago(0, 0, 0, 0, 0)
    )


def script():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    dateFrom = "2022-01-01" # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "optimize_mhid_riverstart" # целевой датасет или база данных для записи

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    ########################################################################################
    ################################################################ Итоговые датафреймы (в них собираются данные)
    control_metrika_stats = pd.DataFrame()
    control_ydirect_stats = pd.DataFrame()
    control_vkads_stats = pd.DataFrame()
    control_mytarget_stats = pd.DataFrame()
    control_click_mytarget = pd.DataFrame()
    control_click_vkads = pd.DataFrame()

    # в названии переменной:
    # yd - яндекс директ
    # ym - метрика
    # vk - вконтакте
    # mt - mytarget
    # дополнительно могут быть указаны:
    # d - метрика для получения данных по директу
    # s - метрика для получения данных по органическому трафику
    # с - данные по аккаунту получаем через систему click.ru

    ##############################################################################
    ############################################################### Яндекс директ
    yd_token = "AgAAAAAQtOqsAAVNXZ-SYSGrW0SWk1obF5HF9GE" # токен директа
    yd_clientLogin = 'mhouse-riverstart' # Логины клиента
    yd_goals = ['221844571','221844344','221844334','62246119','59192935','59192911','41972125','227067489','41929222','41970658','41970718','41970883','41970898','41971087'] # цели
    yd_count_days_per_req = 60 # кол-во дней для 1 запроса к апи директа

    # если есть торговые рк, то указываем данные для подключения к метрике
    ym_d_token = "AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E"
    ym_d_clientLogin = 'mhouse-riverstart'
    ym_d_counter = '23382340'
    ym_d_goals = ['221844571','221844344','221844334','62246119','59192935','59192911','41972125','227067489','41929222','41970658','41970718','41970883','41970898','41971087']

    ##############################################################################
    ############################################################### VK Ads CLICK.RU
    vk_c_uid = '1578482' # айди аккаунта клик ру
    vk_c_tkn =  '578105b12a56e031e91da4addeb6d433' # токен полученный в настройках профиля клик ру

    ##############################################################################
    ############################################################### MyTarget CLICK.RU
    mt_c_uid = '1578482' # айди аккаунта клик ру
    mt_c_tkn =  '578105b12a56e031e91da4addeb6d433' # токен полученный в настройках профиля клик ру

    ##############################################################################
    ############################################################### Яндекс Метрика
    ym_s_token = "AgAAAAAQtOqsAAPqukQYs6QM7UE9lLFXA5D8j6E"
    ym_s_counter = '23382340'
    ym_s_goals = ['221844571','221844344','221844334','62246119','59192935','59192911','41972125','227067489','41929222','41970658','41970718','41970883','41970898','41971087']

    ##############################################################################################################
    #################################################### Сбор данных, закомментить ненужные источники

    ##############################################################################
    ############################################################### Яндекс директ
    fields = 'yd_cpc_control' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
    report_fields = list(config['yandex_direct_names'][fields].split(","))
    direct_dataset = yd.get_data_yd(yd_token, yd_clientLogin, yd_goals, dateFrom, dateTo, yd_count_days_per_req, report_fields) # функция обращается к api директа и собирает данные
    control_ydirect_stats = pd.concat([control_ydirect_stats, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    #############################################################################################
    ########################################################### Сбор данных Метрики - ТОРГОВЫЕ РК

    fields_dim = 'dim_cpc_control_merch' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
    fields_met = 'met_cpc_control_merch' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

    report_fields_dim = config['yandex_metrika_names'][fields_dim]
    report_fields_met = config['yandex_metrika_names'][fields_met]

    # если необходимо получить конверсии, то раскоментируйте следующие строки

    goals_ym = ym.convert_goals_ad(ym_d_goals)
    report_fields_met = report_fields_met + ',' + goals_ym

    metrika_dataset = ym.get_merch_stat(ym_d_counter,ym_d_clientLogin,ym_d_token,dateFrom,dateTo,report_fields_met,report_fields_dim)
    control_ydirect_stats = pd.concat([control_ydirect_stats, metrika_dataset], ignore_index=True)
    
    ##############################################################################
    ############################################################### VK Ads CLICK.RU

    vk_click_dataset = click.click_vk(dateFrom,dateTo,vk_c_uid,vk_c_tkn)
    control_click_vkads = pd.concat([control_click_vkads, vk_click_dataset], ignore_index=True)

    #############################################################################################
    ########################################################### Метрика - другие источники

    fields_dim = 'dim_site_control' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
    fields_met = 'met_site_control' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

    report_fields_dim = config['yandex_metrika_names'][fields_dim]
    report_fields_met = config['yandex_metrika_names'][fields_met]

    # если необходимо получить конверсии, то раскоментируйте следующие строки

    goals_ym = ym.convert_goals_s(ym_d_goals)
    report_fields_met = report_fields_met + ',' + goals_ym

    metrika_dataset = ym.get_ym_stat(ym_d_counter,ym_d_clientLogin,ym_d_token,dateFrom,dateTo,report_fields_met,report_fields_dim)
    control_metrika_stats = pd.concat([control_metrika_stats, metrika_dataset], ignore_index=True)

    ###################################################################################################
    ################################################################ Обработка имен столбцов, для сбора единого датасета
    control_ydirect_stats['TrafficType'] = 'Контекст'
    control_ydirect_stats['service'] = 'YANDEX_DIRECT'
    control_ydirect_stats = control_ydirect_stats.rename(columns={'Login':'account'})

    control_metrika_stats = control_metrika_stats.rename(columns={'ym_s_date':'Date','ym_s_lastsignTrafficSourceName':'TrafficType','ym_s_lastSignSourceEngineName':'service','ym_s_sumVisits':'visits'})
    #control_metrika_stats = control_metrika_stats.rename(columns={'ym_s_lastsignTrafficSourceName':'TrafficType'})
    #control_metrika_stats = control_metrika_stats.rename(columns={'ym_s_lastSignSourceEngineName':'service'})
    control_metrika_stats = control_metrika_stats.drop('Login', axis=1)
    control_metrika_stats.rename(columns=lambda x: x.strip().replace("ym_s_goal", "Conversions_"), inplace=True)

    control_dataset = pd.DataFrame()
    control_dataset = pd.concat([control_dataset, control_click_mytarget], ignore_index=True)
    control_dataset = pd.concat([control_dataset, control_click_vkads], ignore_index=True)
    control_dataset = pd.concat([control_dataset, control_metrika_stats], ignore_index=True)
    control_dataset = pd.concat([control_dataset, control_ydirect_stats], ignore_index=True)
    control_dataset = pd.concat([control_dataset, control_vkads_stats], ignore_index=True)
    control_dataset = pd.concat([control_dataset, control_mytarget_stats], ignore_index=True)

    control_dataset.to_sql("control_dataset", engine, if_exists=mode_write)

    a = "Success!"
    return(a)


operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

operation