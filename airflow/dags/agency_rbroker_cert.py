import sys

sys.path.append('/opt/airflow/')

from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime
from google.oauth2 import service_account


import configparser
import pandas as pd
from sqlalchemy import create_engine
import io
from tapi_yandex_direct import YandexDirect

get_data = DAG(
    "agency_rbroker_cert", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def cert_agency():
    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    # начальная дата
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "agency_cert" # целевой датасет или база данных для записи

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    project_id = 'powerbi4rb'
    json_path = r'/home/alex/airflow/dags/patterns/pbi4rb_gbq.json' # linux

    today = datetime.now().date()
    dateTo = today - timedelta(days=1)
    dateFrom = dateTo - timedelta(days=365)

    ##############################################################################
    ############################################################### Яндекс директ
    token = "AQAAAAAR3H5NAAVNXbtx1UVWI06EpOPYq9aaTpM" # токен директа
    with open(r"/home/alex/airflow/dags/cert_logins.txt", "r") as file:
        logins = file.read().splitlines()

    count_days_per_req = 90 # кол-во дней для 1 запроса к апи директа

    full_dataset = pd.DataFrame(columns=['Login','Month','CampaignId','CampaignType','Cost','Impressions','Clicks','State',"StartDate"])

    with io.open(r"/home/alex/airflow/dags/errors.txt", 'w', encoding='utf-8') as file:
        file.truncate(0)

    for i in logins:
        try:
            fields = 'yd_cert_costs' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
            report_fields = list(config['yandex_direct_names'][fields].split(","))
            direct_dataset = yd.get_data_yd(token, i, '', dateFrom, dateTo, count_days_per_req, report_fields)
            direct_dataset['Login'] = i
            client = YandexDirect(
            # Required parameters:
            access_token=token,
            # If you are making inquiries from an agent account, you must be sure to specify the account login.
            login=i,
            is_sandbox=False,
            # Repeat request when units run out
            retry_if_not_enough_units=False,
            # The language in which the data for directories and errors will be returned.
            language="ru",
            # Repeat the request if the limits on the number of reports or requests are exceeded.
            retry_if_exceeded_limit=True,
            # Number of retries when server errors occur.
            retries_if_server_error=5,
            # Report generation mode: online, offline or auto.
            processing_mode="offline",
            # When requesting a report, it will wait until the report is prepared and download it.
            wait_report=True,
            # Monetary values in the report are returned in currency with an accuracy of two decimal places.
            return_money_in_micros=False,
            # Do not display a line with the report name and date range in the report.
            skip_report_header=True,
            # Do not display a line with field names in the report.
            skip_column_header=False,
            # Do not display a line with the number of statistics lines in the report.
            skip_report_summary=True,
            )

            #Получаем список рекламных кампаний
            body = {
            "method": "get",
            "params": {
                "SelectionCriteria": {},
                "FieldNames": ["Id"]
                },
            }

            list_campaign = []
            report = client.campaigns().post(data=body)
            data = report().extract()
            for campaign in data:
                list_campaign.append("{}".format((campaign['Id'])))

            def compare_list(a, b):
                return not set(a).isdisjoint(b)

            res_df = pd.DataFrame()
            for item in list_campaign:
                body = {
                "method": "get",
                "params": {
                "SelectionCriteria": {
                    "Ids": [item]
                    },
                "FieldNames": ["Id","State","Status","Type","StartDate"]
                    },
                }

                report = client.campaigns().post(data=body)
                data = report().extract()
                df = pd.json_normalize(data)
                res_df = pd.concat([res_df, df], ignore_index=True)

            df_grouped = res_df.groupby(['Id', "StartDate", 'State', 'Status',"Type"]).size().reset_index().drop_duplicates()
            df_grouped = df_grouped[df_grouped['State'] != 'ARCHIVED']
            df_grouped = df_grouped.rename(columns={'Id':'CampaignId'})
            df_grouped = df_grouped.rename(columns={'Type':'CampaignType'})
            df_grouped['Login'] = i

            df_grouped['CampaignId'] = df_grouped['CampaignId'].astype(str)
            direct_dataset['CampaignId'] = direct_dataset['CampaignId'].astype(str)

            # Создаем комбинации идентификаторов и дат из обоих датафреймов
            unique_combinations = pd.MultiIndex.from_product([direct_dataset['Month'].unique(), df_grouped['CampaignId'].unique()], names=['Month', 'CampaignId'])
            merged_df = pd.DataFrame(index=unique_combinations).reset_index()

            # Объединяем с df_grouped
            merged_df = pd.merge(merged_df, df_grouped, on=['CampaignId'], how='left')

            # Объединяем с costs
            merged_df = pd.merge(merged_df, direct_dataset, on=['Month', 'CampaignId'], how='left')

            # Заполняем пропущенные значения в столбце "Cost" нулями
            merged_df['Cost'] = merged_df['Cost'].fillna(0)
            merged_df['Impressions'] = merged_df['Impressions'].fillna(0)
            merged_df['Clicks'] = merged_df['Clicks'].fillna(0)

            # Добавляем недостающие комбинации идентификаторов с нулевыми значениями стоимости
            missing_combinations = merged_df[merged_df['CampaignId'].isnull()][['Month', 'CampaignId']].drop_duplicates()
            missing_combinations = missing_combinations.merge(df_grouped[['CampaignId', 'CampaignType', 'Login']], on='CampaignId', how='left')
            missing_combinations['State'] = df_grouped['State'].unique()[0]
            missing_combinations['Status'] = df_grouped['Status'].unique()[0]
            missing_combinations['Cost'] = 0
            merged_df = pd.concat([merged_df, missing_combinations], ignore_index=True)

            # Сортируем по столбцам "Month", "CampaignId", "CampaignType" и "Login"
            merged_df = merged_df.sort_values(by=['Month', 'CampaignId', 'CampaignType', 'Login']).reset_index(drop=True)

            selected_columns = ['Month', 'CampaignId','Impressions','Clicks', 'Cost', 'CampaignType_x', 'Login_x', 'State', 'Status', 'StartDate']
            merged_df = merged_df.loc[:, selected_columns]
            merged_df = merged_df.rename(columns={'CampaignType_x':'CampaignType'})
            merged_df = merged_df.rename(columns={'Login_x':'Login'})

            full_dataset = pd.concat([full_dataset, merged_df], ignore_index=True)
            #full_dataset = pd.merge(full_dataset, df_grouped[['CampaignId', 'StartDate']], on='CampaignId', how='inner')
        except Exception as e:
            current_datetime = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

            with io.open(r'/home/alex/airflow/dags/errors.txt', 'a', encoding='utf-8') as file:
                file.write(f"{current_datetime} Ошибка при обработке логина {i}: {str(e)}")
            continue

    ###################################################################################################
    ################################################################ Отправляем полученные данные - GBQ
    full_dataset.to_gbq(
            dataset + "." + "clients_costs" + "_stat",  # {dataset_name}.{table_name}
            project_id=project_id,
            if_exists=mode_write,  # fail // append // replace
            credentials=service_account.Credentials.from_service_account_file(json_path)
            )

    a = "Success!"
    return(a)

get_costs_data = PythonOperator(
    task_id = 'data_load_1',
    python_callable=cert_agency,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

get_costs_data