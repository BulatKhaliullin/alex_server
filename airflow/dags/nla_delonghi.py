from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "nla_delonghi", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=datetime(2023, 11, 10),
    schedule_interval='20 2 * * *' # когда запускать задачу, UTC (по МСК +3 часа будет)
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday

    from datetime import datetime, timedelta

    from datetime import date

    from sqlalchemy import create_engine
    from google.oauth2 import service_account

    def period(last_days):
        a=str(yesterday.getYesterday())
        a=datetime.strptime(a, '%Y-%m-%d')
        b=timedelta(days=last_days)
        dateFrom=a-b
        return(dateFrom)

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "nla_delonghi" # целевой датасет или база данных для записи

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    # project_id = 'powerbi4rb'
    # json_path = r"patterns\\pbi4rb_gbq.json" # windows 10
    # json_path = r'/var/home/rb1/!py_script/patterns/pbi4rb_gbq.json' # linux

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    # Получить данные за последние X дней:
    days = 180
    dateFrom = period(days).strftime("%Y-%m-%d")

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'85926832', 
        'token':'y0_AgAAAABUx0tyAAPqugAAAADl1OxwGwxCRcziSH-xGS96O6D1BQGGM24', 
        'conv':['293419570','294320589','297278570'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'select-studio' # тег для куска данных. необходим для объединения с данными по рекламе
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется
    yd_clientLogin = [
        {'login':'delonghi-nla-207', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':['105528115','105528313','105528505','105529309'],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'delonghi'
        },
        {'login':'braun-nla-1', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':['149191399','3069522658'],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'braun'
        },
        {'login':'kenwood-nla-2', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':['3055385347','194415949'],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'kenwood'
        },
    ] 

    ##############################################################################################################
    #################################################### Яндекс Директ
    control_ydirect_stats = pd.DataFrame()

    for i in yd_clientLogin:
        fields = 'yd_nla_report_delonghi' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))

        if i['conv'] == [] or i['conv'] == ['']:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], '', i['dateFrom'], dateTo, 60, report_fields)
        else:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], dateTo, 60, report_fields)
        
        if i['camp_filter'] == [] or i['camp_filter'] == ['']:
            pass
        else:
            direct_dataset = direct_dataset[direct_dataset['CampaignId'].isin(i['camp_filter'])]

        direct_dataset['tag'] = i['tag']
        control_ydirect_stats = pd.concat([control_ydirect_stats, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    ###################################################################################################
    ################################################################ Отправляем полученные данные - POSTGRESQL
    # config = configparser.ConfigParser()
    # config.read("settings.ini")

    # # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    control_ydirect_stats.to_sql("control_ydirect_stats", engine, if_exists=mode_write)

    a = 'Success!'
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3
