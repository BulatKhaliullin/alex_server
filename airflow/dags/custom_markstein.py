from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "custom_markstein", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday
    import re

    from sqlalchemy import create_engine

    dateFrom = "2023-08-20"

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.
    counter = [
        {'counter':'45845343', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':[],
        'dateFrom':dateFrom,
        'dateTo': str(yesterday.getYesterday()),
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'marktstein' # тег для куска данных. необходим для объединения с данными по рекламе
        }
    ]

    # Директ - расходы. Можно удалить если источник не используется

    yd_stats = [
        {'login':'marktstein-next', 
        'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
        'dateFrom':dateFrom,
        'dateTo': str(yesterday.getYesterday()),
        'conv':[],
        'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
        'tag':'marktstein'
        }
    ]

    ##############################################################################################################
    #################################################### Яндекс Метрика
    # Поля для данных метрики

    report_fields_dim = "ym:s:date"
    report_fields_met = "ym:s:visits"

    metrika_stats = pd.DataFrame()

    for i in counter:

        if i['conv'] == [] or i['conv'] == ['']:
            pass
        else:
            goals_ym = ym.convert_goals_s(i['conv'])
            report_fields_met = report_fields_met + ',' + goals_ym

        if i['page_filter'] == [] or i['page_filter'] == ['']:
            metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],i['dateTo'],report_fields_met,report_fields_dim)
            metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_goal", "Conversions_"), inplace=True)
            metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_", ""), inplace=True)
        else:
            values_to_filter = i['page_filter'] #ym:s:startURL
            metrika_dataset = ym.get_ym_stat_filter(i['counter'],"",i['token'],i['dateFrom'],i['dateTo'],report_fields_met,report_fields_dim,values_to_filter)
            metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_goal", "Conversions_"), inplace=True)
            metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_", ""), inplace=True)
        
        metrika_dataset['tag'] = i['tag']
        metrika_stats = pd.concat([metrika_stats, metrika_dataset], ignore_index=True)


    ##############################################################################################################
    #################################################### Яндекс Директ - статистика

    ydirect_stats = pd.DataFrame()

    # Поля для данных директа

    report_fields = "Date,Clicks,Cost"

    for i in yd_stats:
        report_fields = list(report_fields.split(","))
        if i['conv'] == [] or i['conv'] == ['']:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], '', i['dateFrom'], i['dateTo'], 60, report_fields)
        else:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], i['dateTo'], 60, report_fields)
        
        if i['camp_filter'] == [] or i['camp_filter'] == ['']:
            pass
        else:
            direct_dataset = direct_dataset[direct_dataset['CampaignId'].isin(i['camp_filter'])]

        direct_dataset['tag'] = i['tag']
        ydirect_stats = pd.concat([ydirect_stats, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить


    full_dataset = metrika_stats.merge(ydirect_stats, how='left', left_on='date', right_on='Date')
    selected_columns = ['date','visits','Clicks','Cost']
    full_dataset = full_dataset[selected_columns]

    # PostgreSQL - коннект
    dataset = "nla_marktstein"
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)
    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    full_dataset.to_sql("custom_client_table", engine, if_exists=mode_write)

    # отправляет данные в подключенную GS
    def send_to_gs(sh_name, df, json_path, sheet_id):
        import gspread  # для работы с GS
        from gspread_dataframe import set_with_dataframe  # функция отправки таблицы в GS
        from google.oauth2.credentials import Credentials  # для авторизации в Google
        
        #-# авторизация и подключение к листу (создание его, при отсутствии)
        gc = gspread.service_account(filename=r'/home/alex/airflow/dags/patterns/python-data-to-sheets-857407b48471.json')  # местоположение ключа oauth2
        sh = gc.open_by_key(sheet_id)  # id таблицы

        # sh_name = название листа, на который будут записаны данные 
        # df = данные, которые нужно отправить
        # если указанного листа нет, то он будет создан
        try:
            worksheet = sh.worksheet(sh_name)
        except:
            worksheet = sh.add_worksheet(title=sh_name,
                                        rows="10",
                                        cols="100")

        # очистка листа
        worksheet.clear()

        # запись на лист
        set_with_dataframe(worksheet=worksheet,
                        dataframe=df,
                        include_index=False,
                        include_column_header=True,
                        resize=True)

    # ключ Json от GCP (oauth2)
    json_path = 'python_sheets.json'

    # ID таблицы GS
    sheet_id = '114ExPIDQ5lY0PXr1Be3wkldTM_9CxKml9f3F4T9nTOc'

    # Название листа, на который будут записаны данные
    sheet_name = 'dataset'

    send_to_gs(sh_name=sheet_name,  # название листа, в который будут записаны данные
            df=full_dataset,  # таблица, которая будет отправлена в GS
            json_path=json_path,  # путь к json авторизации
            sheet_id=sheet_id)  # идентификатор таблицы GS

    a = 'Success!'
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3