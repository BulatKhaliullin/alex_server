from airflow import DAG
from datetime import timedelta

from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
from datetime import datetime

import sys
import configparser
import pandas as pd
from sqlalchemy import create_engine

get_data = DAG(
    "custom_ret", # Название задачи в airflow. по умолчанию - идентификатор клиента_control
    start_date=days_ago(0, 0, 0, 0, 0) # когда запускать задачу
    )

def script():
    ###########################################################################################
    ############################################################# Импорт библиотек

    sys.path.append(r'/home/alex/airflow/dags/patterns')
    sys.path.append(r'/home/alex/airflow/dags/')
    config = configparser.ConfigParser()
    config.read(r'/home/alex/airflow/dags/__fields.ini')
    config_db = configparser.ConfigParser()
    config_db.read(r'/home/alex/airflow/dags/__settings.ini')

    # папка patterns должна лежать рядом с данным скриптом
    import patterns.yd as yd
    import patterns.ym as ym
    import patterns.click as click
    import patterns.yesterday as yesterday
    import requests

    from sqlalchemy import create_engine

    ###########################################################################################
    ################################################################ укажите параметры скрипта
    dateTo = str(yesterday.getYesterday()) #конечная дата
    dataset = "nla_dataset" # целевой датасет или база данных для записи

    mode_write = "replace" # перезапись данных
    #mode_write = "append" # добавление данных

    # GoogleBigQuery - коннект
    # project_id = 'powerbi4rb'
    # json_path = r"patterns\\pbi4rb_gbq.json" # windows 10
    # json_path = r'/var/home/rb1/!py_script/patterns/pbi4rb_gbq.json' # linux

    # PostgreSQL - коннект
    engine = create_engine("postgresql://" + config_db['postgresql']['username'] + ':' + config_db['postgresql']['password'] + '@' + config_db['postgresql']['host'] + ':' + config_db['postgresql']['port'] + "/" + dataset)

    dateFrom = "2023-01-01"
    ret_control_ydirect_stats = pd.DataFrame(columns=['Date','CampaignName','CampaignId','Cost','AdGroupName'])
    ret_control_ydirect_stats.to_sql("ret_control_ydirect_stats", engine, if_exists=mode_write)
    # Директ - расходы. Можно удалить если источник не используется

    yd_clientLogin = [
        {'login':'technopride-nla-204', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':[''],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'-'
        },
        {'login':'ret2-nla-195', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':[''],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'-'
        },
        {'login':'ret-nla', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':[''],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'-'
        },
        {'login':'Hausdorf2-nla-185', 
            'token':'y0_AgAAAABpV9EgAAVNXQAAAADqcmHhThBAwyjTSiSp-TR_e_OGfzGHSKw',
            'dateFrom':dateFrom,
            'conv':[''],
            'camp_filter':[], # Укажите рекламные кампании, которые относятся к текущему клиенту. В противном случае будут получены все кампании
            'tag':'-'
        }
    ]

    ##############################################################################################################
    #################################################### Яндекс Директ

    for i in yd_clientLogin:
        fields = 'yd_nla_report_ret' # набор полей которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
        report_fields = list(config['yandex_direct_names'][fields].split(","))

        if i['conv'] == [] or i['conv'] == ['']:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], '', i['dateFrom'], dateTo, 30, report_fields)
        else:
            direct_dataset = yd.get_data_yd(i['token'], i['login'], i['conv'], i['dateFrom'], dateTo, 30, report_fields)
        
        if i['camp_filter'] == [] or i['camp_filter'] == ['']:
            pass
        else:
            direct_dataset = direct_dataset[direct_dataset['CampaignId'].isin(i['camp_filter'])]

        direct_dataset = direct_dataset.astype({'CampaignId':'int64'})
        ret_control_ydirect_stats = pd.concat([ret_control_ydirect_stats, direct_dataset], ignore_index=True) # кладем полученный датасет в общий, чтобы сохранить

    # Метрика - источники/конверсии. Обязательный пункт, может быть несколько счетчиков.

    ret_control_metrika_stats = pd.DataFrame(columns=['date','cross_device_last_yandex_direct_clickTrafficSource','cross_device_last_yandex_direct_clickDirectClickOrder','counterID','cross_device_last_yandex_direct_clickDirectBannerGroupName','Доход по цели (Заказ создан)','Доход по цели (Заказ оплачен)'])
    ret_control_metrika_stats.to_sql("ret_control_metrika_stats", engine, if_exists=mode_write)

    # в переменной conv в данном скрипте - первая конверсия: Заказ создан, вторая конверсия: Заказ оплачен
    counter = [
        {'counter':'26755035', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866658','273866659'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'Ag-russia' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'17564362', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262369100','262369101'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'asko-russia' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'54981802', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866649','273866650'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'Bonecrusher-shop' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'11806957', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262863186','262863187'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'bs-rus' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'11599613', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['280296285','280296286'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'dedietrich-home' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'45555603', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262178806','262178807'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'elicahome' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'18704212', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['261917396','261917397'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'elux-ru' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'17567473', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866660','273866661'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'faber-studio' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'45850923', 
        'token':'y0_AgAAAABpLrrSAAPqugAAAADs9O0yX6I6j2_1SDu-HxKUbCgtcTt_b9g', 
        'conv':['274967103','274967104'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'falmec-home' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'40825049', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866647','273866648'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'gorenje-home' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'27434912', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['261917377','261917378'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'gorenje-ru' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'45201903', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['274967106','274967107'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'graude-store' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'44195889', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866633','273866634'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'insinkerator-shop' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'57404926', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866656','273866657'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'jackys-store' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'7522579', 
        'token':'y0_AgAAAABpLrrSAAPqugAAAADs9O0yX6I6j2_1SDu-HxKUbCgtcTt_b9g', 
        'conv':['273866613','273866614'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'Kaiser-studio' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'24912398', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['274967108','274967109'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'korting-dealer' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'80352145', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866643','273866644'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'Krona-home' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'14490490', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262369061','262369062'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'kuppers' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'11820094', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['261917400','261917401'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'l-rus' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'48033788', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262369079','262369080'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'maunfeld-home' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'33943214', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866639','273866640'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'mitsubishi-japan' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'11820151', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['280213173','280213174'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'ml-russia' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'8359381', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['261917398','261917399'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'nf-rus' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'90180512', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866641','273866642'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'niagara-russia' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'36861110', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866615','273866616'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'omoikiri-design' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'15873064', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866635','273866636'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'sim-studio' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'1873522', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['262369102','262369103'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'sm-rus' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'30747708', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866637','273866638'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'teka-ru' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'48619403', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866653','273866654'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'vestfrost-store' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'11773411', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['238923754','238923755'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'hausdorf' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'93606200', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['298184463','298184464'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'kuppersbusch-centre' # тег для куска данных. необходим для объединения с данными по рекламе
        },

        {'counter':'23687830', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['288952315','288952316'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'hitachi-holodilnik' # тег для куска данных. необходим для объединения с данными по рекламе
        },
        
        {'counter':'32239984', 
        'token':'y0_AgAAAABpV9EgAAPqugAAAADqcnCYqqxzLYvdRuO1s_wMgEpYGJWwa1k', 
        'conv':['273866662','273866663'],
        'dateFrom':dateFrom,
        'page_filter': "", #"EXISTS(ym:s:startURL=*'*tariff*')", #OR EXISTS(ym:pv:URL=*'*about*')" - выбрать визиты, где был просмотр URL содержащих tariffs. 
        'tag':'asko-shop' # тег для куска данных. необходим для объединения с данными по рекламе
        }
    ]

    ##############################################################################################################
    #################################################### Яндекс Метрика
    import time  # Добавляем модуль time
    from tapi_yandex_metrika import exceptions

    for i in counter:
        while True:  # Бесконечный цикл для повторных попыток
            try:
                fields_dim = 'dim_nla_ret' # набор параметров, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника
                fields_met = '' # набор метрик, которые требуется получить. для создания нового набора добавьте в файл data_fields.ini новый список полей для соответствующего источника

                report_fields_dim = config['yandex_metrika_names'][fields_dim]

                if i['conv'] == [] or i['conv'] == ['']:
                    pass
                else:
                    goals_revenue_ym = ym.convert_goals_revenue_s(i['conv'])
                    report_fields_met = goals_revenue_ym

                if i['page_filter'] == [] or i['page_filter'] == ['']:
                    metrika_dataset = ym.get_ym_stat(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim)
                    metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_goal", "Conversions_"), inplace=True)
                    metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_", ""), inplace=True)
                else:
                    values_to_filter = i['page_filter'] #ym:s:startURL
                    metrika_dataset = ym.get_ym_stat_filter(i['counter'],"",i['token'],i['dateFrom'],dateTo,report_fields_met,report_fields_dim,values_to_filter)
                    metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_goal", "Conversions_"), inplace=True)
                    metrika_dataset.rename(columns=lambda x: x.strip().replace("ym_s_", ""), inplace=True)
                
                # Получение списка столбцов с исходными названиями
                original_columns = metrika_dataset.columns

                # Создание словаря для новых названий столбцов
                new_columns = {}
                for col in original_columns:
                    if col.startswith('Conversions_' + i['conv'][0]):
                        new_col = "Доход по цели (Заказ создан)"
                    else:
                        new_col = col
                    new_columns[col] = new_col

                # Переименование столбцов
                metrika_dataset.rename(columns=new_columns, inplace=True)

                new_columns = {}
                for col in original_columns:
                    if col.startswith('Conversions_' + i['conv'][1]):
                        new_col = "Доход по цели (Заказ оплачен)"
                    else:
                        new_col = col
                    new_columns[col] = new_col

                # Переименование столбцов
                metrika_dataset.rename(columns=new_columns, inplace=True)

                metrika_dataset['tag'] = i['tag']
                ret_control_metrika_stats = pd.concat([ret_control_metrika_stats, metrika_dataset], ignore_index=True)

                break
            except exceptions.YandexMetrikaClientError as e:
                if e.code == 400 and "Запрос слишком сложный" in e.message:
                    print("Ошибка: Запрос слишком сложный. Повторная попытка через 5 секунд...")
                    time.sleep(5)  # Подождать 5 секунд перед повторной попыткой
                else:
                    print("Произошла другая ошибка:", e)
                    break  # Выход из бесконечного цикла при другой ошибке

    #############################################################################################################
    ############################################################ Динамика по продажам из CRM

    # URL, по которому находится CSV файл
    url = "http://t50.su/automate/orders/dynamic.csv"

    # Отправляем GET-запрос для загрузки файла с сервера
    response = requests.get(url)

    # Проверяем, успешно ли загружен файл
    if response.status_code == 200:
        # Создаем временный файл для сохранения данных
        with open(r'/home/alex/airflow/dags/temporary.csv', "wb") as temp_file:
            temp_file.write(response.content)

        try:
            # Загружаем данные из временного файла с явным указанием разделителя (;)
            ret_dynamic_orders_data = pd.read_csv(r'/home/alex/airflow/dags/temporary.csv', encoding="cp1251", delimiter=';', header=0)

            # Удаляем временный файл
            import os
            os.remove(r'/home/alex/airflow/dags/temporary.csv')

            # Теперь переменная 'ret_dynamic_orders_data' содержит данные из CSV файла с правильным разделителем
            print(ret_dynamic_orders_data.head())
        except pd.errors.ParserError:
            print("Ошибка при чтении файла")
    else:
        print("Не удалось загрузить файл. Код состояния:", response.status_code)

    ret_control_metrika_stats.to_sql("ret_control_metrika_stats", engine, if_exists=mode_write)
    ret_control_ydirect_stats.to_sql("ret_control_ydirect_stats", engine, if_exists=mode_write)
    ret_dynamic_orders_data.to_sql("ret_dynamic_orders_data", engine, if_exists=mode_write)

    a = 'Success!'
    return(a)

# Для каждой функции копируем блок operation и вписываем функцию в python_callable

operation = PythonOperator(
    task_id = 'data_load',
    python_callable=script,
    dag=get_data
    )

# Если функций несколько, раскомментировать часть кода ниже для определения очередности выполнения

operation #>> operation2 >> operation3